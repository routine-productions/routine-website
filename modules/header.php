<div class="Header-Wrap">
    <div class="Header">
        <header class="Site-Header">
            <div class="Logo">
                <svg>
                    <use xlink:href="#rootine"></use>
                </svg>
            </div>
            <div class="Nav">
                <ul>
                    <li class="">
                        <button>Наши кейсы</button>
                    </li>
                    <li>
                        <button>Услуги</button>
                    </li>
                    <li>
                        <button>Контактная информация</button>
                    </li>
                    <li class="To-Routine"><a href=""><span>routine.framework</span></a></li>
                </ul>
            </div>

        </header>
    </div>

    <div class="Header-Title">
        <h1>Делаем прибыльные сайты для бизнеса</h1>
        <span class="Go-Cases">
            <button>
                <span>Наши кейсы</span>
            </button>
        </span>

    </div>
    <div class="Go-Scroll">
        <svg>
            <use xlink:href="#mouse"></use>
        </svg>
    </div>

</div>
