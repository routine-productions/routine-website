<div class="Go-Top">
    <svg>
        <use xlink:href="#arrow"></use>
    </svg>
    <span>Наверх</span>
</div>
<footer>

    <div class="Logo">
        <svg>
            <use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#rootine"></use>
        </svg>
    </div>
    <ul class="Nav">
        <li>
            <button>Наши кейсы</button>
        </li>
        <li>
            <button>Услуги</button>
        </li>
        <li>
            <button>Контактная информация</button>
        </li>
    </ul>
    <div class="Routine"><a href=""><span>routine.framework</span></a></div>

</footer>
<div class="Copyright">
    <span>© Routine Productions, 2016 г.</span>
</div>
