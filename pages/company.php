<section class="Cases">
    <h2>Наши кейсы</h2>

    <div class="Previous">
        <svg>
            <use xlink:href="#arrow"></use>
        </svg>
    </div>
    <div class="Next">
        <svg>
            <use xlink:href="#arrow"></use>
        </svg>
    </div>

    <ul class="Slider">
        <li>
            <div class="Slide-Info">
                <div class="Slide-Head">
                    <svg>
                        <defs>
                            <linearGradient id="autoban-1" gradientUnits="userSpaceOnUse" x1="25.0454" y1="118.7827"
                                            x2="136.5421"
                                            y2="118.7827">
                                <stop offset="0" style="stop-color:#FFA53F"/>
                                <stop offset="1" style="stop-color:#FF3800"/>
                            </linearGradient>
                            <linearGradient id="autoban-2" gradientUnits="userSpaceOnUse" x1="48.4916" y1="51.4368"
                                            x2="108.8222"
                                            y2="51.4368">
                                <stop offset="0" style="stop-color:#FFA53F"/>
                                <stop offset="1" style="stop-color:#FF3800"/>
                            </linearGradient>
                            <linearGradient id="autoban-3" gradientUnits="userSpaceOnUse" x1="35.6538" y1="86.7516"
                                            x2="119.9616"
                                            y2="86.7516">
                                <stop offset="0" style="stop-color:#FFA53F"/>
                                <stop offset="1" style="stop-color:#FF3800"/>
                            </linearGradient>
                            <linearGradient id="autoban-4" gradientUnits="userSpaceOnUse" x1="61.3294" y1="14.0591"
                                            x2="97.6829"
                                            y2="14.0591">
                                <stop offset="0" style="stop-color:#FFA53F"/>
                                <stop offset="1" style="stop-color:#FF3800"/>
                            </linearGradient>
                            <linearGradient id="autoban-5" gradientUnits="userSpaceOnUse" x1="2.4604" y1="139.8333"
                                            x2="159.1271"
                                            y2="139.8333">
                                <stop offset="0" style="stop-color:#FFA53F"/>
                                <stop offset="1" style="stop-color:#FF3800"/>
                            </linearGradient>

                            <linearGradient id="autoban-6" gradientUnits="userSpaceOnUse" x1="10.5947" y1="160.9868"
                                            x2="167.6075"
                                            y2="160.9868">
                                <stop offset="0" style="stop-color:#FFA53F"/>
                                <stop offset="1" style="stop-color:#FF3800"/>
                            </linearGradient>

                            <linearGradient id="autoban-7" gradientUnits="userSpaceOnUse" x1="10.5947" y1="69.4542"
                                            x2="167.6075"
                                            y2="69.4542">
                                <stop offset="0" style="stop-color:#FFA53F"/>
                                <stop offset="1" style="stop-color:#FF3800"/>
                            </linearGradient>

                            <linearGradient id="autoban-8" gradientUnits="userSpaceOnUse" x1="10.5947" y1="69.4542"
                                            x2="167.6075"
                                            y2="69.4542">
                                <stop offset="0" style="stop-color:#FFA53F"/>
                                <stop offset="1" style="stop-color:#FF3800"/>
                            </linearGradient>


                        </defs>
                        <use xlink:href="#autoban-logo"></use>
                    </svg>
                    <h2>Автобан
                        <small>Группа компаний</small>
                    </h2>
                </div>
            </div>
            <div class="Slide-Footer">
                <h3>Сайт группы компаний «Автобан»</h3>
                <a href="autoban33.ru"><span>autoban33.ru</span>
                    <svg>
                        <use xlink:href="#external-link"></use>
                    </svg>
                </a>
            </div>
        </li>
    </ul>
</section>
<div class="Services-Wrap">
    <section class="Services">
        <h2>Наши услуги</h2>
    </section>
    <ul class="Services-List">
        <li>
            <div class="Service">
                <div class="Landing"></div>
                <div class="Text">
                    <h3>Посадочная страница</h3>

                    <div class="Hr"></div>
                    <p>
                        Один из самых эффективных способов быстро продвинуть один товар или услугу в интернете. Самые
                        высокие уровни конверсии посетителя в клиента.
                    </p>
                </div>
                <div class="Hr-Down"></div>
            </div>
        </li>
        <li>
            <div class="Service">
                <div class="Cutaway"></div>
                <div class="Text">
                    <h3>Сайт-визитка</h3>

                    <div class="Hr"></div>
                    <p>
                        Лучший способ заявить о себе и презентовать свои услуги клиентам. Просто поделитесь с ними
                        ссылкой и всё остальное сделает Ваш сайт.
                    </p>
                </div>
                <div class="Hr-Down"></div>
            </div>
        </li>
        <li>
            <div class="Service">
                <div class="Corporal"></div>
                <div class="Text">
                    <h3>Корпоративный сайт</h3>

                    <div class="Hr"></div>
                    <p>
                        Полноценное представительство Вашей компании в интернете.
                        Обязательный атрибут любого успешного бизнеса.
                    </p>
                </div>
                <div class="Hr-Down"></div>
            </div>
        </li>
        <li>
            <div class="Service">
                <div class="Catalogue"></div>
                <div class="Text">
                    <h3>Каталог</h3>

                    <div class="Hr"></div>
                    <p>
                        Покажите ассортимент и цены Вашим клиентам в удобном и понятном виде.
                    </p>
                </div>
                <div class="Hr-Down"></div>
            </div>
        </li>
        <li>
            <div class="Service">
                <div class="Shop"></div>
                <div class="Text">
                    <h3>Интернет-магазин</h3>

                    <div class="Hr"></div>
                    <p>
                        Никакой арендной платы за торговые площади, раздутого штата продавцов и ограничения на торговлю
                        только в пределах города. Интернет-магазин всегда выгоднее традиционной торговой точки. </p>
                </div>
                <div class="Hr-Down"></div>
            </div>
        </li>
        <li>
            <div class="Service">
                <div class="Style">
                    <svg>
                        <defs>
                            <linearGradient id="dark-1" gradientUnits="userSpaceOnUse" x1="139.8196" y1="109.9122"
                                            x2="139.9198" y2="109.9122">
                                <stop offset="0" style="stop-color:#64A902"/>
                                <stop offset="0.5" style="stop-color:#05590C"/>
                                <stop offset="1" style="stop-color:#64A902"/>
                            </linearGradient>
                            <linearGradient id="dark-2" gradientUnits="userSpaceOnUse" x1="364.628" y1="109.8403"
                                            x2="364.6288" y2="109.8403">
                                <stop offset="0" style="stop-color:#64A902"/>
                                <stop offset="0.5" style="stop-color:#05590C"/>
                                <stop offset="1" style="stop-color:#64A902"/>
                            </linearGradient>
                            <linearGradient id="dark-3" gradientUnits="userSpaceOnUse" x1="136.992" y1="166.0881"
                                            x2="136.992" y2="55.1982">
                                <stop offset="0" style="stop-color:#64A902"/>
                                <stop offset="1" style="stop-color:#05590C"/>
                            </linearGradient>
                            <linearGradient id="dark-4" gradientUnits="userSpaceOnUse" x1="367.4846" y1="165.2905"
                                            x2="367.4846" y2="55.7059">
                                <stop offset="0" style="stop-color:#64A902"/>
                                <stop offset="1" style="stop-color:#05590C"/>
                            </linearGradient>

                            <linearGradient id="dark-5" gradientUnits="userSpaceOnUse" x1="174.0209" y1="170.6504"
                                            x2="357.9189" y2="67.2078">
                                <stop offset="0" style="stop-color:#05590C"/>
                                <stop offset="0.5" style="stop-color:#64A902"/>
                                <stop offset="1" style="stop-color:#05590C"/>
                            </linearGradient>

                            <linearGradient id="dark-6" gradientUnits="userSpaceOnUse" x1="148.9823" y1="152.5792"
                                            x2="344.2127" y2="42.7621">
                                <stop offset="0" style="stop-color:#05590C"/>
                                <stop offset="0.5" style="stop-color:#64A902"/>
                                <stop offset="1" style="stop-color:#05590C"/>
                            </linearGradient>

                            <linearGradient id="dark-7" gradientUnits="userSpaceOnUse" x1="155.5565" y1="164.2548"
                                            x2="350.7035" y2="54.4847">
                                <stop offset="0" style="stop-color:#05590C"/>
                                <stop offset="0.5" style="stop-color:#64A902"/>
                                <stop offset="1" style="stop-color:#05590C"/>
                            </linearGradient>
                            <linearGradient id="dark-8" gradientUnits="userSpaceOnUse" x1="215.2508" y1="147.1192"
                                            x2="238.4811" y2="123.8889">
                                <stop offset="0" style="stop-color:#64A902"/>
                                <stop offset="1" style="stop-color:#05590C"/>
                            </linearGradient>
                        </defs>
                        <use xlink:href="#dark-logo"></use>
                    </svg>
                </div>
                <div class="Text">
                    <h3>Фирменный стиль</h3>

                    <div class="Hr"></div>
                    <p>
                        Джентельменский набор из всего необходимого для развивающегося бизнеса: логотип, визитки,
                        бланки, макеты для лайтбоксов и билбордов и много другое в одном наборе.</p>
                </div>
                <div class="Hr-Down"></div>
            </div>
        </li>
    </ul>
</div>
